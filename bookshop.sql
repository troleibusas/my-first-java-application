-- MariaDB dump 10.19  Distrib 10.4.24-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: bookshop
-- ------------------------------------------------------
-- Server version	10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `author` varchar(50) NOT NULL,
  `publisher` varchar(50) NOT NULL,
  `year` smallint(6) NOT NULL,
  `isbn` bigint(20) NOT NULL,
  `language` varchar(50) NOT NULL,
  `price` varchar(10) NOT NULL,
  `genre` enum('DRAMA','CLASSIC','ADVENTURE','DETECTIVE','FANTASY','FICTION','HORROR','ROMANCE','SCI_FI','BIOGRAPHY','EDUCATIONAL','LANGUAGES') NOT NULL DEFAULT 'DRAMA',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (21,'Testing the Clearing','Just testing if these forms clear themselves.','Me Me','Nunya Bidniss Books',2022,3407656217695,'anglų','23.59','DRAMA'),(24,'Made Up Stuff','Lorem ipsum??????????????','Jolly Rancher','Garbage Factory',1967,7864565786781,'anglų','12.22','DRAMA'),(25,'Refreshing fields','Ooga booga? Booga booga!!!!','The Cavemen','Paleo Publishing',1999,3604618751374,'Babajų','39.99','DRAMA'),(27,'What is going on?','Something is happening. Find out, what.','Confused Guy','Mystery Cave',1999,6431976798551,'anglų','12.99','DETECTIVE'),(30,'Poltava','Istorinė knyga, nukelianti į pačią XVIII a. pradžią. Jos kulminacija – 1709 m. įvykęs Poltavos mūšis, bet šis pasakojimas ne tik apie didelį mūšį. Jis apima pirmąją Didžiojo Šiaurės karo (1700–1721) pusę ir supažindina su Europa XVIII a. išvakarėse. Gali būti, kad tai didelio pasakojimo pradžia, taip pat neblogas įvadas tiems, kurie susidomėtų XVIII a. istorija. Knygoje sutiksite ne tik Rusijos carą Petrą I, bet ir Švedijos karalių Karolį XII, taip pat Lenkijos karalių Augustą II. Tai ir Lietuvos Didžiosios Kunigaikštystės istorija, su kuria atsiranda bloga nuojauta, pasitvirtinsianti amžiaus pabaigoje. Tai ir Ukrainos istorija, be kurios neįmanoma suprasti šiuolaikinio Rusijos ir Ukrainos konflikto.','Robertas Petrauskas','Protų kovos',2022,9786099606330,'lietuvių','25.99','EDUCATIONAL'),(31,'Vokiečių kalba per 30 dienų','Vokiečių kalba – greitai ir efektyviai. Įgytas A2 kalbos lygis sutekia galimybę susikalbėti kasdienėse situacijose ir bendrauti įvairiomis bendrosiomis temomis. Garsinės pamokų užduotys įrašytos 3-juose audio failuose, knygoje pateikta nuoroda jų atsisiuntimui.','Angelika Beck','Margi raštai',2017,9789986094777,'lietuvių','11.68','LANGUAGES'),(34,'Dievų miškas','Balys Sruoga (1896–1947) – lietuvių literatūros klasikas. Be jo neįmanoma įsivaizduoti XX a. pirmosios pusės Lietuvos kultūrinio gyvenimo. Rašytojo atsiminimų knyga „Dievų miškas“ vertinama kaip vienas originaliausių Europos memuaristikos kūrinių apie nacių koncentracijos stovyklas. Pirmą kartą atsiminimai išleisti 1957 m., autoriui jau mirus. 2005 m. režisierius Algimantas Puipa kūrinio motyvais pastatė kino filmą.','Balys Sruoga','Baltos lankos',2021,9789955236993,'lietuvių','4.29','EDUCATIONAL'),(35,'Doriano Grėjaus portretas','Tai vienintelis išspausdintas (1980 m.) anglų kilmės dramaturgo, poeto Oscar Wilde romanas, kuris ano meto Anglijoje amžininkų buvo vertinamas labai prieštaringai, o autoriui pelnė skandalingą šlovę. Knygoje pasakojama istorija apie Dorianą Grėjų, patrauklų ir turtingą jaunuolį, kurio gyvenimą sugriauna pasaulio purvas ir niekšybės. O galbūt tai dvasinis paties jaunuolio nuopuolis? Kai Grėjaus angeliška išvaizda nutapoma portrete, jaunuolis pradeda svajoti, kad realiame pasaulyje jo grožis visuomet išliktų amžinas, o negailestingą senėjimą „išgyventų“ jaunuolį vaizduojantis portretas. Kai Dorianas įsitraukia į nuodėmingą gyvenimą, jo portretas iš tiesų pasikeičia: jaunuolio atvaizdas paveiksle tampa perkreiptas ir šlykštus.','Oscar Wilde','Jotema',2021,9786094900143,'lietuvių','17.99','FICTION'),(36,'Vakarų fronte nieko naujo','Ericho Maria Remarque‘o (1898-1970) romanas „Vakarų fronte nieko naujo“ (1929), viena iškiliausių ir populiariausių XX a. pasaulinės literatūros viršūnių, laikomas bene išraiškingiausiu „prarastosios kartos“ rašytojų manifestu ir įtaigiausiu kūriniu apie karą.Paprastomis, natūraliomis ir lakoniškomis stilistinėmis priemonėmis Remarque‘as užčiuopė ir įprasmino pagrindinio herojaus Pauliaus Boimerio ir jo bendraamžių, atsidūrusių mirties akivaizdoje, dvasines būsenas.','Erich Maria Remarque','Jotema',2021,9789955138594,'lietuvių','15.99','CLASSIC'),(38,'Tūla','Jurgio Kunčino (1947–2002) romanas „Tūla“ priskiriamas prie ryškiausių lietuvių literatūros kūrinių, pasirodžiusių per du pastaruosius dešimtmečius. Fatališka meilė, Vilniaus gatveles užgulusi sovietmečio pilkuma, nerimo ir liūdesio slegiamas inteligentiškas jaunuolis – iš tokių vaizdų susiklostė geriausias J. Kunčino romanas.','Jurgis Kunčinas','LRS leidykla',2020,9789986398523,'lietuvių','12.39','CLASSIC'),(39,'Svetimas','Albert\'o Camus romanas Svetimas (1942) rašytojui pelnė pasaulinį pripažinimą. Pagrindinio veikėjo Merso santykis su supančiu pasauliu pažeidžia visuomenės taisykles ir paverčia jį „svetimu\". Tai knyga apie neįveikiamą individo vienatvę inertiško abejingumo, absurdo akivaizdoje.','Albert Camus','Baltos lankos',2018,9789955232933,'lietuvių','10.99','CLASSIC'),(40,'Gyvulių ūkis','Tai neabejotinai svarbiausias grožinės politinės satyros kūrinys, parašytas XX a. Britanijoje. Šioje G. Orvelo knygoje pasitelkus gyvulinės pasakėčios tradiciją, jungiamas satyrinis ir politinis įtūžis su gyvybingu mitinio kūrinio amžinumu. Tai viena iš didžiųjų politinių alegorijų, o jos pasakojama istorija yra apie tai, kaip socialinė revoliucija (vieno Anglijos ūkininko gyvuliai susimokę sukyla prieš šeimininką, išveja jį ir pradeda ūkyje kurti savarankišką gyvenimą) virto diktatūra ir išdavyste.','George Orwell','Jotema',2018,9789955137610,'lietuvių','8.29','CLASSIC'),(41,'Nusikaltimas ir bausmė','Romane rašytojas gilinasi į nusikaltimo psichologiją, žvelgia į žudiką, gyvenantį tarsi normalų gyvenimą, į jo sielą, kaip jis atsiteisia už padarytą nusikaltimą.','Fiodoras Dostojevskis','Margi raštai',2008,9789986092148,'lietuvių','11.49','CLASSIC'),(42,'Kafka on the Shore','Kafka on the Shore displays one of the world\'s great storytellers at the peak of his powers.Here we meet a teenage boy, Kafka Tamura, who is on the run, and Nakata, an aging simpleton who is drawn to Kafka for reasons that he cannot fathom. As their paths converge, acclaimed author Haruki Murakami enfolds readers in a world where cats talk, fish fall from the sky, and spirits slip out of their bodies to make love or commit murder, in what is a truly remarkable journey.','Haruki Murakami','Random House LCC US',2006,9781400079278,'anglų','23.29','FICTION'),(43,'Time Management','Superinė knyga TAU! :c','Steve Martin','GA Publishing',2022,9798201769079,'anglų','24.29','EDUCATIONAL'),(47,'Testing the WEB thing','Does this work? Hopefully. Just gonna stretch for words here. I need summer, so tired, and I bet everyone is as tired as me. What a year.','Miss Boombalatty','Nunya Bidniss Books',2022,3407676698745,'anglu','16.99','EDUCATIONAL'),(51,'Original title','Holy moly','Some Made Up Guy','Dummy Books',2017,1326597846728,'angl?','10.99','FICTION');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `commenterName` varchar(30) NOT NULL,
  `bookISBN` bigint(20) NOT NULL,
  `commentID` int(10) NOT NULL AUTO_INCREMENT,
  `rootCommentID` int(10) DEFAULT NULL,
  `commentContent` varchar(500) NOT NULL,
  PRIMARY KEY (`commentID`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES ('admin',3407656217695,1,NULL,'Testing the comments. This book is great.'),('admin',3407656217695,2,NULL,'One more?'),('admin',3407656217695,4,2,'please work?'),('admin',3407656217695,5,2,'I don\'t get it.'),('admin',3407656217695,6,NULL,'Omg this thing is working (somewhat)'),('admin',3407656217695,7,6,'It\'s 1am I am really tired. That\'s what you get'),('admin',9789986094777,8,NULL,'Ich bin ąč Jahre alt.'),('admin',9789986094777,9,8,'12*'),('admin',9781400079278,10,NULL,'Traumatising'),('admin',9789986092148,11,NULL,'Good one'),('Kazimieras',7864565786781,12,NULL,'Unlike this book, I am real'),('Kazimieras',7864565786781,13,NULL,'Oi aš gi nemoku angliškai, aš tik balandis'),('admin',3407656217695,15,7,'whoa'),('admin',3407656217695,16,1,'b00ks'),('Gibait Logistics, UAB',3407656217695,24,NULL,'My turn to test'),('Gibait Logistics, UAB',3407656217695,25,24,'REplying to myself'),('Gibait Logistics, UAB',9786094900143,26,NULL,'Gr8 b00k'),('Jonas',9789955236993,29,NULL,'vau'),('Jonas',3407656217695,30,NULL,'and me too'),('Chris',9781400079278,35,10,'Totally'),('zuikutis',3407676698745,36,NULL,'Testing comment posting thru web'),('kiskutis',3407676698745,37,36,'awesome, it works'),('Kitasyk, UAB',9789955232933,38,NULL,'Lovinam'),('Kitasyk, UAB',9781400079278,39,NULL,'Puiki knyga padovanoti kolektyvui jei norite kreivu zvilgsniu'),('Stanislava',9789986092148,40,NULL,'Gera knyga, patiko');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderedbooks`
--

DROP TABLE IF EXISTS `orderedbooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderedbooks` (
  `itemID` int(11) NOT NULL AUTO_INCREMENT,
  `orderID` int(10) NOT NULL,
  `ISBN` bigint(13) NOT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderedbooks`
--

LOCK TABLES `orderedbooks` WRITE;
/*!40000 ALTER TABLE `orderedbooks` DISABLE KEYS */;
INSERT INTO `orderedbooks` VALUES (109,764647822,9789955138594),(110,764647822,9789955232933),(120,165744505,9786099606330),(121,165744505,9789955137610),(122,165744505,9789955232933),(123,165744505,3407676698745),(124,165744505,7864565786781),(125,165744505,9789955137610),(126,165744505,9786094900143),(127,313460961,9781400079278),(128,313460961,9781400079278),(129,313460961,9781400079278),(130,313460961,9781400079278),(131,313460961,9781400079278),(132,313460961,9781400079278),(133,313460961,9781400079278),(134,313460961,9781400079278),(135,1736222484,9786099606330),(136,1736222484,9789986094777),(137,1375004005,9789955138594),(138,1319388454,9781400079278),(139,1319388454,9789955232933);
/*!40000 ALTER TABLE `orderedbooks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `orderNumber` int(10) NOT NULL,
  `customerID` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `paymentMethod` enum('CASH','CARD','PAYPAL','BANK') NOT NULL,
  `dateCreated` date NOT NULL,
  `totalPrice` varchar(7) NOT NULL,
  PRIMARY KEY (`orderNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (165744505,'informacija@kitasyk.lt','Kitasyk, UAB\nPramonės g. 16 64861 Panevėžys\ninformacija@kitasyk.lt\n861237956','BANK','2022-04-19','100.76'),(313460961,'informacija@kitasyk.lt','Kitasyk, UAB\nPramonės g. 16 64861 Panevėžys\ninformacija@kitasyk.lt\n861237956','CARD','2022-04-19','186.32'),(764647822,'grybas@post.lt','Jonas Šalkauskas\nGruzinų g. 16 03768 Vilnius\ngrybas@post.lt\n867734076','PAYPAL','2022-04-19','31.27'),(1319388454,'informacija@parazitas.lt','Parazitas, VšĮ\nTrimitų g. 35 21388 Vilnius\ninformacija@parazitas.lt\n853498774','CARD','2022-04-19','34.28'),(1375004005,'gelytes123@gmail.com','Stanislava Ivanova\nŠaltinio g. 31 Didžiosios Lapės 54420\ngelytes123@gmail.com\n867946877','CARD','2022-04-19','15.99'),(1515040064,'asesu@balandis.lt','Kazimieras Petrauskas\nGiedraičių g. 78 01947 Vilnius\nasesu@balandis.lt\n867644076','CARD','2022-04-19','164.73'),(1736222484,'info@gelda.lt','Gelda, UAB\nLinkmenų g. 16 01344 Vilnius\ninfo@gelda.lt\n867546814','CARD','2022-04-19','37.67');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `phoneNumber` varchar(20) NOT NULL DEFAULT '0',
  `emailAddress` varchar(50) NOT NULL,
  `VATCode` varchar(10) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `dateCreated` date NOT NULL,
  `dateModified` date NOT NULL,
  `role` enum('CUSTOMER','EMPLOYEE','ADMIN') NOT NULL DEFAULT 'CUSTOMER',
  `address` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`emailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('','admin',NULL,'admin','2022-03-08','2022-04-19','ADMIN','','admin',NULL,'1992-03-01',NULL),('867644076','asesu@balandis.lt','6429826','valdone123','2022-03-29','2022-04-04','CUSTOMER','Giedraičių g. 78 01947 Vilnius','Kazimieras','Petrauskas','1967-05-08',NULL),('869482874','balbib@zebra.lt','','kny_gos','2022-04-01','2022-04-01','CUSTOMER','Klevų g. 7 59243 Balbieriškis',NULL,NULL,NULL,'Balbieriškio Biblioteka, BĮ'),('866679876','biblioteka@gencom.lt','354668','password123','2022-04-17','2022-04-17','CUSTOMER','Medžių g. 687 12679 Vilnius',NULL,NULL,NULL,'generic company'),('867698526','dmitry@gibaitlogistics.lt','13584578','Asmyliupasta','2022-03-09','2022-03-09','CUSTOMER','Parko g. 13 01468 Vilnius',NULL,NULL,NULL,'Gibait Logistics, UAB'),('863486498','dont@mebro.com','','6timesplatinum','2022-04-02','2022-04-02','CUSTOMER','V. Grybo g. 55, Raseiniai 60114','Chris','Rea','1951-03-04',NULL),('867946877','gelytes123@gmail.com','','gertruda1999','2022-04-01','2022-04-01','CUSTOMER','Šaltinio g. 31 Didžiosios Lapės 54420','Stanislava','Ivanova','1949-02-15',NULL),('868453219','ggiryte2007@gmail.com','','123456789','2022-04-03','2022-04-03','CUSTOMER','Suvalkų g. 33 91668 Klaipėda','Gabrielė','Girytė','2007-08-18',NULL),('867734076','grybas@post.lt','','netavoreikalas','2022-04-17','2022-04-19','CUSTOMER','Gruzinų g. 16 03768 Vilnius','Jonas','Šalkauskas','1991-08-21',NULL),('864578943','info@bublikas.lt','139758','mmmmniamniam','2022-04-01','2022-04-01','CUSTOMER','P. Višinskio g. 51 44156 Kaunas',NULL,NULL,NULL,'Bublikas, ŽŪB'),('867546814','info@gelda.lt','3416835','12345','2022-03-29','2022-04-01','CUSTOMER','Linkmenų g. 16 01344 Vilnius',NULL,NULL,NULL,'Gelda, UAB'),('861237956','informacija@kitasyk.lt','','kitasyk','2022-04-03','2022-04-03','CUSTOMER','Pramonės g. 16 64861 Panevėžys',NULL,NULL,NULL,'Kitasyk, UAB'),('853498774','informacija@parazitas.lt','','tarakonai','2022-03-30','2022-03-30','CUSTOMER','Trimitų g. 35 21388 Vilnius',NULL,NULL,NULL,'Parazitas, VšĮ'),('864975945','jzhdanovich@gmail.com','','slaptazodziokeitimas','2022-04-02','2022-04-02','CUSTOMER','Kelmijos Sodų 84-oji g. 02217 Vilnius','Jozita','Ždanovič','1976-01-30',NULL),('64865','testukas',NULL,'testukas','2022-04-18','2022-04-18','CUSTOMER','tstuksd','bimbim','bimbirim','1976-04-27',NULL),('862372016','troleibusas13@gmail.com','','12345','2022-04-01','2022-04-01','EMPLOYEE','Lazdynu g. 24-9 03140 Vilnius','Viktorija','Butkeviciute','2000-06-18',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-19 22:13:19
