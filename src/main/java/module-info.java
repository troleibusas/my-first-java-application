/**
 *
 */
module com.example.bookshop {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.java;
    requires spring.context;
    requires spring.core;
    requires spring.web;
    requires java.persistence;
    requires com.google.gson;
    requires org.json;


    opens com.example.bookshop to javafx.fxml;
    exports com.example.bookshop.model;
    exports com.example.bookshop;
    exports com.example.bookshop.formControllers;
    opens com.example.bookshop.formControllers to javafx.fxml;
}