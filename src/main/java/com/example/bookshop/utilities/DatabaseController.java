package com.example.bookshop.utilities;

import com.example.bookshop.model.*;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;

public class DatabaseController {
    public static Connection connectToDatabase() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String DB_URL = "jdbc:mysql://localhost:3306/bookshop";
            String USER = "root";
            String PASSWORD = "";
            connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (Exception e){
            e.printStackTrace();
        }
        return connection;
    }

    public static void disconnectFromDatabase(Connection connection, Statement statement){
        try {
            if (connection != null) connection.close();
            if (statement != null) statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Book> getBooks(String columnName, String request) {
        ArrayList<Book> bookList = new ArrayList<Book>();
        Connection connection = DatabaseController.connectToDatabase();
        String selectQuery;
        if (request != "") {
            selectQuery = "SELECT * FROM books where " + columnName + " = \"" + request + "\"";
        }
        else { selectQuery = "SELECT * FROM books"; }
        Statement statement = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                bookList.add(new Book (resultSet.getString(2), Genre.valueOf(resultSet.getString(10)),
                        resultSet.getString(3), resultSet.getString(4),
                        resultSet.getString(5), resultSet.getShort(6),
                        resultSet.getLong(7), resultSet.getString(8), resultSet.getString(9)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, statement);
        return bookList;
    }

    public static ArrayList<User> getUsers (String columnName, String request) {
        Connection connection = DatabaseController.connectToDatabase();
        String selectQuery;
        if (request != "") {
            selectQuery = "SELECT * FROM users where " + columnName + " = \"" + request + "\"";
        }
        else { selectQuery = "SELECT * FROM users"; }

        Statement statement = null;
        Person person; Company company; ArrayList<User> users = new ArrayList<User>();
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                if (resultSet.getString(12) != null){
                    company = new Company(resultSet.getString(1), resultSet.getString(2),
                            resultSet.getString(3), resultSet.getString(4), resultSet.getDate(5).toLocalDate(),
                            resultSet.getDate(6).toLocalDate(), Role.valueOf(resultSet.getString(7)),
                            resultSet.getString(8), resultSet.getString(12));
                    users.add(company);
                }
                else{
                    person = new Person(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3),
                            resultSet.getString(4), resultSet.getDate(5).toLocalDate(), resultSet.getDate(6).toLocalDate(),
                            Role.valueOf(resultSet.getString(7)), resultSet.getString(8), resultSet.getString(9),
                            resultSet.getString(10), resultSet.getDate(11).toLocalDate());
                    users.add(person);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, statement);
        return users;
    }

    public static User getUserByLogin (String email, String password) {
        Connection connection = DatabaseController.connectToDatabase();
        String selectQuery = "SELECT * FROM users where emailAddress = \"" + email + "\" AND " +
                "password = \"" + password + "\"";

        Statement statement = null;
        Person person; Company company; User user = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                if (resultSet.getString(12) != null){
                    company = new Company(resultSet.getString(1), resultSet.getString(2),
                            resultSet.getString(3), resultSet.getString(4), resultSet.getDate(5).toLocalDate(),
                            resultSet.getDate(6).toLocalDate(), Role.valueOf(resultSet.getString(7)),
                            resultSet.getString(8), resultSet.getString(12));
                    user = company;
                }
                else{
                    person = new Person(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3),
                            resultSet.getString(4), resultSet.getDate(5).toLocalDate(), resultSet.getDate(6).toLocalDate(),
                            Role.valueOf(resultSet.getString(7)), resultSet.getString(8), resultSet.getString(9),
                            resultSet.getString(10), resultSet.getDate(11).toLocalDate());
                    user = person;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, statement);
        return user;
    }

    public static ArrayList<Comment> getComments (long bookISBN) {
        ArrayList<Comment> commentList = new ArrayList<Comment>();
        String selectQuery;
        Connection connection = DatabaseController.connectToDatabase();
        if (bookISBN == 0) selectQuery = "SELECT * FROM comments";
        else selectQuery = "SELECT * FROM comments WHERE bookISBN = " + bookISBN;
        Statement statement = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                commentList.add(new Comment (resultSet.getString(1), resultSet.getLong(2),
                        resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, statement);
        return commentList;
    }

    public static ArrayList<Order> getOrders (String user){
        ArrayList<Order> orderList = new ArrayList<Order>();
        String selectQuery;
        Connection connection = DatabaseController.connectToDatabase();
        if (user.equals("")) selectQuery = "SELECT * FROM orders";
        else selectQuery = "SELECT * FROM orders WHERE customerID = \"" + user + "\"";
        Statement statement = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                orderList.add(new Order (resultSet.getString(2), resultSet.getInt(1),
                        resultSet.getDate(5).toLocalDate(), resultSet.getString(3),
                        resultSet.getString(6), PaymentMethod.valueOf(resultSet.getString(4))));
            }
            orderList.forEach(order -> getOrderItems(order));

        } catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, statement);
        return orderList;
    }

    public static Order getOrderByNumber (int number) {
        Order order = new Order();
        Connection connection = DatabaseController.connectToDatabase();
        String selectQuery = "SELECT * FROM orders WHERE orderNumber = \"" + number + "\"";
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectQuery);

            if(resultSet.next()){
                order = new Order(resultSet.getString(2), resultSet.getInt(1),
                        resultSet.getDate(5).toLocalDate(), resultSet.getString(3),
                        resultSet.getString(6), PaymentMethod.valueOf(resultSet.getString(4)));
                getOrderItems(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, statement);
        return order;
    }

    public static Order getOrderItems(Order order){
        ArrayList<OrderedBook> orderedItems = new ArrayList<>();
        Connection connection = DatabaseController.connectToDatabase();
        String selectQuery = "SELECT * FROM orderedbooks WHERE orderID = " + order.getOrderNumber();
        Statement statement = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                orderedItems.add(new OrderedBook (resultSet.getInt(2), resultSet.getLong(3), resultSet.getInt(1)));
            }
            order.setOrderedItems(orderedItems);
        } catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, statement);
        System.out.println(order);
        return order;
    }

    public static OrderedBook getOrderedBook(int id){
        OrderedBook orderedBook = null;
        Connection connection = DatabaseController.connectToDatabase();
        String selectQuery = "SELECT * FROM orderedbooks WHERE itemID = " + id;
        Statement statement = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectQuery);
            if (resultSet.next()) {
                orderedBook = new OrderedBook (resultSet.getInt(2), resultSet.getLong(3),
                        resultSet.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("damn "+ orderedBook.getOrderNumber());
        DatabaseController.disconnectFromDatabase(connection, statement);
        return orderedBook;
    }

    public static void deleteUser (String email) {
        Connection connection = DatabaseController.connectToDatabase();
        String deleteQuery;
        deleteQuery = "DELETE FROM users WHERE emailAddress = \"" + email + "\"";
        PreparedStatement preparedStatement = null;
        try{
            preparedStatement = connection.prepareStatement(deleteQuery);
            preparedStatement.execute();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement);
    }

    public static void deleteBook (String ISBN) {
        Connection connection = DatabaseController.connectToDatabase();
        String deleteQuery;
        deleteQuery = "DELETE FROM books WHERE ISBN = \"" + ISBN + "\"";
        PreparedStatement preparedStatement = null;
        try{
            preparedStatement = connection.prepareStatement(deleteQuery);
            preparedStatement.execute();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement);
    }

    public static void addBook (Book book) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String insertQuery = "INSERT INTO books(`name`, `description`,`author`,`publisher`,`year`,`isbn`,`language`,`price`,`genre`) VALUES(?,?,?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
        preparedStatement.setString(1, book.getName());
        preparedStatement.setString(2, book.getDescription());
        preparedStatement.setString(3, book.getAuthor());
        preparedStatement.setString(4, book.getPublisher());
        preparedStatement.setShort(5, book.getYear());
        preparedStatement.setLong(6, book.getISBN());
        preparedStatement.setString(7, book.getLanguage());
        preparedStatement.setString(8, String.valueOf(book.getPrice()));
        preparedStatement.setString(9, String.valueOf(book.getGenre()));
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement);
    }

    public static void addUser (User user, boolean isCompany) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String insertQuery = "INSERT INTO users(`phoneNumber`, `emailAddress`,`VATCode`,`password`,`dateCreated`," +
                "`dateModified`,`role`,`address`,`name`,`surname`,`birthDate`,`title`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
        if (isCompany){
            Company company = (Company) user;
            preparedStatement.setString(1, company.getPhoneNumber());
            preparedStatement.setString(2, company.getEmailAddress());
            preparedStatement.setString(3, company.getVATCode());
            preparedStatement.setString(4, company.getPassword());
            preparedStatement.setDate(5, Date.valueOf(company.getDateCreated()));
            preparedStatement.setDate(6, Date.valueOf(company.getDateModified()));
            preparedStatement.setString(7, company.getRole().toString());
            preparedStatement.setString(8, company.getAddress());
            preparedStatement.setString(9, null);
            preparedStatement.setString(10, null);
            preparedStatement.setString(11, null);
            preparedStatement.setString(12, company.getTitle());
            preparedStatement.execute();
        }
        else{
            Person person = (Person) user;
            preparedStatement.setString(1, person.getPhoneNumber());
            preparedStatement.setString(2, person.getEmailAddress());
            preparedStatement.setString(3, person.getVATCode());
            preparedStatement.setString(4, person.getPassword());
            preparedStatement.setDate(5, Date.valueOf(person.getDateCreated()));
            preparedStatement.setDate(6, Date.valueOf(person.getDateModified()));
            preparedStatement.setString(7, person.getRole().toString());
            preparedStatement.setString(8, person.getAddress());
            preparedStatement.setString(9, person.getName());
            preparedStatement.setString(10, person.getSurname());
            preparedStatement.setDate(11, Date.valueOf(person.getBirthDate()));
            preparedStatement.setString(12, null);
            preparedStatement.execute();
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement);
    }

    public static void addOrder(Order order) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String insertQuery = "INSERT INTO orders(`orderNumber`, `customerID`,`address`,`paymentMethod`,`dateCreated`," +
                "`totalPrice`) VALUES(?,?,?,?,?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
        preparedStatement.setInt(1, order.getOrderNumber());
        preparedStatement.setString(2, order.getCustomerID());
        preparedStatement.setString(3, order.getAddress());
        preparedStatement.setString(4, order.getPaymentMethod().toString());
        preparedStatement.setDate(5, Date.valueOf(order.getDateCreated()));
        preparedStatement.setString(6, order.getTotalPrice().toString());
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement);

        order.getOrderedItems().forEach(orderedBook -> {
            try {
                addBooksToOrder(orderedBook);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static void addComment (Comment comment, boolean isReply) throws SQLException {
        String insertQuery;
        Connection connection = DatabaseController.connectToDatabase();
        if (!isReply) insertQuery = "INSERT INTO comments(`commenterName`, `commentContent`,  `bookISBN`) VALUES(?,?,?)";
        else insertQuery = "INSERT INTO comments(`commenterName`, `commentContent`,  `bookISBN`, `rootCommentID`) VALUES(?,?,?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
        preparedStatement.setString(1, comment.getCommenterName());
        preparedStatement.setString(2, comment.getCommentContent());
        preparedStatement.setLong(3, comment.getBookISBN());
        if (isReply) preparedStatement.setInt(4, comment.getRootCommentID());
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement);
    }

    public static void addBooksToOrder(OrderedBook orderedBook) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String insertQuery = "INSERT INTO orderedBooks(`orderID`, `ISBN`) VALUES(?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
        preparedStatement.setInt(1, orderedBook.getOrderNumber());
        preparedStatement.setLong(2, orderedBook.getISBN());
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement);
    }

    public static ArrayList<Book> searchBook (String textSearch, String genreSearch, String priceSearch) {
        ArrayList<Book> bookList = new ArrayList<Book>();
        Connection connection = DatabaseController.connectToDatabase();
        String selectQuery;
        if ((!textSearch.equals("")) && (genreSearch.equals("")) && (priceSearch.equals("0.0"))) {
            selectQuery = "SELECT * FROM books WHERE name" + " LIKE \"%" + textSearch + "%\"" + " OR " +
                    "author" + " LIKE \"%" + textSearch + "%\"" + " OR " +
                    "publisher" + " LIKE \"%" + textSearch + "%\"";
        }
        else if ((!textSearch.equals("")) && (!genreSearch.equals("")) && (priceSearch.equals("0.0"))){
            selectQuery = "SELECT * FROM books WHERE (name" + " LIKE \"%" + textSearch + "%\"" + " OR " +
                    "author" + " LIKE \"%" + textSearch + "%\"" + " OR " +
                    "publisher" + " LIKE \"%" + textSearch + "%\")" + " AND " +
                    "genre = \"" + genreSearch + "\"";
        }
        else if ((!textSearch.equals("")) && (genreSearch.equals("")) && (!priceSearch.equals("0.0"))){
            selectQuery = "SELECT * FROM books WHERE (name" + " LIKE \"%" + textSearch + "%\"" + " OR " +
                    "author" + " LIKE \"%" + textSearch + "%\"" + " OR " +
                    "publisher" + " LIKE \"%" + textSearch + "%\")" + " AND " +
                    "price < " + priceSearch;
        }
        else if ((!textSearch.equals("")) && (!genreSearch.equals("")) && (!priceSearch.equals("0.0"))){
            selectQuery = "SELECT * FROM books WHERE (name" + " LIKE \"%" + textSearch + "%\"" + " OR " +
                    "author" + " LIKE \"%" + textSearch + "%\"" + " OR " +
                    "publisher" + " LIKE \"%" + textSearch + "%\")" + " AND " +
                    "(genre = \"" + genreSearch + "\")" + " AND " +
                    "price < " + priceSearch;
        }
        else if ((textSearch.equals("")) && (!genreSearch.equals("")) && (priceSearch.equals("0.0"))){
            selectQuery = "SELECT * FROM books WHERE genre = \"" + genreSearch + "\"";
        }
        else if ((textSearch.equals("")) && (!genreSearch.equals("")) && (!priceSearch.equals("0.0"))){
            selectQuery = "SELECT * FROM books WHERE genre = \"" + genreSearch + "\"" + " AND " +
                    "price < " + priceSearch;
        }
        else if ((textSearch.equals("")) && (genreSearch.equals("")) && (!priceSearch.equals("0.0"))){
            selectQuery = "SELECT * FROM books WHERE price < " + priceSearch;
        }

        else { selectQuery = "SELECT * FROM books"; }


        Statement statement = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                bookList.add(new Book (resultSet.getString(2), Genre.valueOf(resultSet.getString(10)),
                        resultSet.getString(3), resultSet.getString(4),
                        resultSet.getString(5), resultSet.getShort(6),
                        resultSet.getLong(7), resultSet.getString(8), resultSet.getString(9)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, statement);
        return bookList;

    }

    public static void deleteComment(int commentID) {
        Connection connection = DatabaseController.connectToDatabase();
        String deleteQuery;
        deleteQuery = "DELETE FROM comments WHERE rootCommentID = " + commentID + " OR " +
                "commentID = " + commentID;
        ;
        PreparedStatement preparedStatement = null;
        try{
            preparedStatement = connection.prepareStatement(deleteQuery);
            preparedStatement.execute();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement);
    }

    public static void deleteOrder(int orderNumber){
        Connection connection = DatabaseController.connectToDatabase();
        String deleteOrderQuery; String deleteOrderedItemsQuery;
        deleteOrderQuery = "DELETE FROM orders WHERE orderNumber = " + orderNumber;
        deleteOrderedItemsQuery = "DELETE FROM orderedbooks WHERE orderID = " + orderNumber;
        PreparedStatement preparedStatement = null;
        try{
            preparedStatement = connection.prepareStatement(deleteOrderQuery);
            preparedStatement.execute();
            preparedStatement = connection.prepareStatement(deleteOrderedItemsQuery);
            preparedStatement.execute();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement);
    }

    public static void deleteOrderedBook(OrderedBook orderedBook) {
        Connection connection = DatabaseController.connectToDatabase();
        String deleteQuery = "DELETE FROM orderedbooks WHERE itemID = " + orderedBook.getBookID();
        PreparedStatement preparedStatement = null;
        try{
            preparedStatement = connection.prepareStatement(deleteQuery);
            preparedStatement.execute();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement);
    }
}
