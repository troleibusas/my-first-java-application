package com.example.bookshop.webControllers;

import com.example.bookshop.model.Order;
import com.example.bookshop.model.OrderedBook;
import com.example.bookshop.utilities.DatabaseController;
import com.example.bookshop.utilities.LocalDateGsonSerializer;
import com.google.gson.GsonBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Controller
public class OrderedItemsWebController {
    @RequestMapping(value = "orderedItems/getOrderedItems/{orderNumber}/", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String getOrderedItems(@PathVariable(name = "orderNumber") int orderNumber) {
        Order order = DatabaseController.getOrderByNumber(orderNumber);
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateGsonSerializer());
        return builder.create().toJson(order.getOrderedItems());
    }

    @RequestMapping(value = "orderedItems/deleteOrderedItem/{id}/", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String deleteOrderedItem(@PathVariable(name = "id") int id) {
        OrderedBook orderedBook = DatabaseController.getOrderedBook(id);
        DatabaseController.deleteOrderedBook(orderedBook);
        return "Deleted book #" + id + " from order #" + orderedBook.getOrderNumber() +".";
    }
}
