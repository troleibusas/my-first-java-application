package com.example.bookshop.webControllers;

import com.example.bookshop.model.Order;
import com.example.bookshop.model.OrderedBook;
import com.example.bookshop.model.PaymentMethod;
import com.example.bookshop.utilities.DatabaseController;
import com.example.bookshop.utilities.LocalDateGsonSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONObject;
import org.json.JSONArray;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Properties;

@Controller
public class OrderWebController {
    @RequestMapping(value = "orders/getOrders", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String getOrders() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateGsonSerializer());
        return builder.create().toJson(DatabaseController.getOrders(""));
    }

    @RequestMapping(value = "orders/getOrdersByUser/{customerID}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String getOrdersByUser(@PathVariable(name = "customerID") String customerID) {
        ArrayList<Order> orders = DatabaseController.getOrders(customerID);
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateGsonSerializer());
        return builder.create().toJson(orders);
    }

    /*@RequestMapping(value = "orders/createOrder", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String createOrder (@RequestBody Order newOrderInfo) throws SQLException {

    }*/

    @RequestMapping(value = "orders/deleteOrder/{orderNumber}/", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String deleteOrder(@PathVariable(name = "orderNumber") String orderNumber) {
        DatabaseController.deleteOrder(Integer.parseInt(orderNumber));
        return "Deleted order #" + orderNumber + ".";
    }
}
