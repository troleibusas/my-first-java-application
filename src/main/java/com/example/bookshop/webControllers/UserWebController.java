package com.example.bookshop.webControllers;

import com.example.bookshop.model.Company;
import com.example.bookshop.model.Person;
import com.example.bookshop.model.Role;
import com.example.bookshop.model.User;
import com.example.bookshop.utilities.DatabaseController;
import com.example.bookshop.utilities.LocalDateGsonSerializer;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Properties;

@Controller
public class UserWebController {

    @RequestMapping(value = "users/validateUser", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String validateUser(@RequestBody String userInfo) {
        Gson parser = new Gson();
        Properties data = parser.fromJson(userInfo, Properties.class);
        User user = DatabaseController.getUserByLogin(data.getProperty("login"), data.getProperty("psw"));
        if (user == null) {
            return "Check login information.";
        }
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateGsonSerializer());
        return builder.create().toJson(user);
    }

    @RequestMapping(value = "users/createUser", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String createWebUser(@RequestBody String newUserInfo) throws SQLException {
        Gson parser = new Gson();
        Properties data = parser.fromJson(newUserInfo, Properties.class);
        if (data.getProperty("title") == null) {
            Person person = new Person(data.getProperty("phoneNumber"),
                    data.getProperty("emailAddress"),
                    data.getProperty("VATCode"),
                    data.getProperty("password"),
                    Role.valueOf(data.getProperty("role")),
                    data.getProperty("address"),
                    data.getProperty("name"),
                    data.getProperty("surname"),
                    LocalDate.parse(data.getProperty("birthDate")));
            DatabaseController.addUser(person, false);
            return "Successfully created a person";
        }
        else {
            Company company = new Company(data.getProperty("phoneNumber"),
                    data.getProperty("emailAddress"),
                    data.getProperty("VATCode"),
                    data.getProperty("password"),
                    Role.valueOf(data.getProperty("role")),
                    data.getProperty("address"),
                    data.getProperty("title"));
            DatabaseController.addUser(company, true);
            return "Successfully created a company";
        }
    }

    @RequestMapping(value = "users/getUserByEmail/{email}/", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String getUserByEmail(@PathVariable(name = "email") String emailAddress){
        User user = DatabaseController.getUsers("emailAddress", emailAddress).get(0);
        if (user.getClass() == new Person().getClass()){
            return ((Person)user).toString();
        }
        else {
            return ((Company)user).toString();
        }
    }

    @RequestMapping(value = "users/getUsers/", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String getUsers(){
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateGsonSerializer());
        return builder.create().toJson(DatabaseController.getUsers("",""));
    }

    @RequestMapping(value = "users/updateUsers/", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String updateUser(){
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateGsonSerializer());
        return builder.create().toJson(DatabaseController.getUsers("",""));
    }

    /*@RequestMapping(value = "users/editUser/{email}/", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String editUser(@RequestBody String updatedUserInfo, @PathVariable(name = "email") String emailAddress) throws SQLException {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateGsonSerializer());
        Gson gson = builder.create();
        User updatedUser = gson.fromJson(updatedUserInfo, User.class);
        updatedUser.setDateModified(LocalDate.now());
        if (updatedUser.getClass() == new Person().getClass()){
            DatabaseController.addUser((Person)updatedUser, false);
            DatabaseController.deleteUser(emailAddress);
        }
        else {
            DatabaseController.addUser((Company)updatedUser, true);
            DatabaseController.deleteUser(emailAddress);
        }
        return "Updated user " + emailAddress + ".";
    }*/

    @RequestMapping(value = "users/deleteUser/{email}/", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String deleteUser(@PathVariable(name = "email") String emailAddress){
        DatabaseController.deleteUser(emailAddress);
        return "Deleted user " + emailAddress + ".";
    }
}

