package com.example.bookshop.webControllers;

import com.example.bookshop.model.*;
import com.example.bookshop.utilities.DatabaseController;
import com.example.bookshop.utilities.LocalDateGsonSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Properties;

@Controller
public class BookWebController {

    @RequestMapping(value = "books/createBook", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String createBook(@RequestBody String newBookInfo) throws SQLException {
        Gson parser = new Gson();
        Properties data = parser.fromJson(newBookInfo, Properties.class);
        Book book = new Book(data.getProperty("name"),
                Genre.valueOf(data.getProperty("genre")),
                data.getProperty("description"),
                data.getProperty("author"),
                data.getProperty("publisher"),
                Short.parseShort(data.getProperty("year")),
                Long.parseLong(data.getProperty("ISBN")),
                data.getProperty("language"),
                data.getProperty("price"));
        DatabaseController.addBook(book);
        GsonBuilder builder = new GsonBuilder();
        return "New book: \n" + builder.create().toJson(book);
    }

    @RequestMapping(value = "books/getBookByISBN/{ISBN}/", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String getBookByISBN(@PathVariable(name = "ISBN") String ISBN) {
        Book book = DatabaseController.getBooks("ISBN", ISBN).get(0);
        GsonBuilder builder = new GsonBuilder();
        return builder.create().toJson(book);
    }

    @RequestMapping(value = "books/getBooks/", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String getBooks() {
        GsonBuilder builder = new GsonBuilder();
        return builder.create().toJson(DatabaseController.getBooks("", ""));
    }

    /*@RequestMapping(value = "books/editBook/{ISBN}/", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String editBook(@RequestBody String updatedBookInfo, @PathVariable(name = "ISBN") String ISBN) throws SQLException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Book updatedBook = gson.fromJson(updatedBookInfo, Book.class);
        DatabaseController.deleteBook(ISBN);
        DatabaseController.addBook(updatedBook);
        return "Updated book " + ISBN + ":\n" + builder.create().toJson(updatedBook);
    }*/

    @RequestMapping(value = "books/deleteBook/{ISBN}/", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String deleteBook(@PathVariable(name = "ISBN") String ISBN) {
        DatabaseController.deleteBook(ISBN);
        return "Deleted book " + ISBN + ".";
    }
}
