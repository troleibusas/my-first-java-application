package com.example.bookshop.webControllers;

import com.example.bookshop.model.Comment;
import com.example.bookshop.utilities.DatabaseController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

@Controller
public class CommentWebController {
    @RequestMapping(value = "comments/getCommentsByISBN/{ISBN}/", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String getCommentsByISBN(@PathVariable(name = "ISBN") String ISBN) {
        ArrayList<Comment> comments = DatabaseController.getComments(Long.parseLong(ISBN));
        GsonBuilder builder = new GsonBuilder();
        return builder.create().toJson(comments);
    }

    @RequestMapping(value = "comments/getComments/", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String getComments() {
        GsonBuilder builder = new GsonBuilder();
        return builder.create().toJson(DatabaseController.getComments(0));
    }

    @RequestMapping(value = "comments/createComment", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String createComment(@RequestBody String newCommentInfo) throws SQLException {
        Gson parser = new Gson();
        Properties data = parser.fromJson(newCommentInfo, Properties.class);
        Comment comment = new Comment(data.getProperty("commenterName"),
                Long.parseLong(data.getProperty("bookISBN")),
                Integer.parseInt(data.getProperty("rootCommentID")),
                data.getProperty("commentContent"));
        if (comment.getRootCommentID() == 0) DatabaseController.addComment(comment, false);
        else DatabaseController.addComment(comment, true);
        GsonBuilder builder = new GsonBuilder();
        ArrayList<Comment> comments = DatabaseController.getComments(0);
        Comment newComment = comments.get(comments.size()-1);
        return "New comment: \n" + builder.create().toJson(newComment);
    }

    @RequestMapping(value = "comments/deleteComment/{id}/", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public String deleteComment(@PathVariable(name = "id") String id) {
        DatabaseController.deleteComment(Integer.parseInt(id));
        return "Deleted comment #" + id + ".";
    }
}
