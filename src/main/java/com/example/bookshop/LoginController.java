
package com.example.bookshop;

import com.example.bookshop.model.Order;
import com.example.bookshop.model.ShoppingCart;
import com.example.bookshop.model.User;
import com.example.bookshop.utilities.DatabaseController;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController extends Application {
    private static User loggedInUser;
    private static ShoppingCart shoppingCart;
    private static Order order;
    @FXML
    public TextField loginField;
    @FXML
    public TextField passwordField;
    @FXML
    public Button loginButton;
    @FXML
    public Button signupButton;

    public static void setLoggedInUser(User user) {
        LoginController.loggedInUser = user;
    }

    public static User getLoggedInUser() {
        return loggedInUser;
    }

    public static ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public static Order getOrder() {
        return order;
    }

    public static void setOrder(Order order) {
        LoginController.order = order;
    }

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("loginPage.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    public void validateAndLogin(ActionEvent actionEvent) throws IOException {
        User user = DatabaseController.getUserByLogin(loginField.getText(), passwordField.getText());
        if (user == null) {
            showLoginError();
        }
        else {
            loggedInUser = DatabaseController.getUsers("emailAddress", loginField.getText()).get(0);
            shoppingCart = new ShoppingCart();
            FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("mainPage.fxml"));
            Parent parent = fxmlLoader.load();
            Scene scene = new Scene(parent);
            Stage stage = (Stage)loginButton.getScene().getWindow();
            stage.setTitle("Book Store");
            stage.setScene(scene);
            stage.show();
        }
    }

    public void loadSignUpForm(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("signupPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)loginButton.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }

    public void showLoginError() throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("loginPopup.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Klaida");
        stage.setScene(scene);
        stage.show();
    }
}