package com.example.bookshop.formControllers;

import com.example.bookshop.LoginController;
import com.example.bookshop.model.Order;
import com.example.bookshop.utilities.DatabaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class UserOrdersController implements Initializable {
    @FXML
    public ListView orderList;
    @FXML
    public ListView itemsList;
    @FXML
    public Button returnButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        orderList.getItems().addAll(DatabaseController.getOrders(LoginController.getLoggedInUser().getEmailAddress()));
        orderList.getSelectionModel().selectFirst();
        showOrderItems();
    }

    public void showOrderItems() {
        Order selectedOrder = (Order)orderList.getSelectionModel().getSelectedItem();
        itemsList.getItems().clear();
        itemsList.getItems().addAll(selectedOrder.getOrderedItems());
    }

    public void returnToMainPage(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("mainPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)returnButton.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }


}
