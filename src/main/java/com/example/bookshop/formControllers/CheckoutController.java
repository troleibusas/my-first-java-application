package com.example.bookshop.formControllers;

import com.example.bookshop.LoginController;
import com.example.bookshop.model.*;
import com.example.bookshop.utilities.DatabaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.ResourceBundle;

public class CheckoutController implements Initializable {
    @FXML
    private  ArrayList<Book> books;
    @FXML
    private PaymentMethod paymentMethod;
    @FXML
    private String address;
    @FXML
    public ListView bookList;
    @FXML
    public MenuItem deleteBookFromCart;
    @FXML
    public RadioButton cardRadio;
    @FXML
    public RadioButton cashRadio;
    @FXML
    public RadioButton bankRadio;
    @FXML
    public RadioButton paypalRadio;
    @FXML
    public Text priceText;
    @FXML
    public Button confirmButton;
    @FXML
    public Button returnButton;
    @FXML
    public TextArea addressField;

    public void confirmAndReturn(ActionEvent actionEvent) throws SQLException, IOException {
        if (cardRadio.isSelected()) paymentMethod = PaymentMethod.CARD;
        if (cashRadio.isSelected()) paymentMethod = PaymentMethod.CASH;
        if (bankRadio.isSelected()) paymentMethod = PaymentMethod.BANK;
        if (paypalRadio.isSelected()) paymentMethod = PaymentMethod.PAYPAL;
        String totalPrice = String.valueOf(LoginController.getShoppingCart().calculateTotalPrice());
        String customerID = LoginController.getLoggedInUser().getEmailAddress();
        address = addressField.getText();
        Order order = new Order(books, customerID, address, totalPrice, paymentMethod);
        DatabaseController.addOrder(order);
        LoginController.getShoppingCart().clear();

        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("mainPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)returnButton.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    public void returnToMainScreen(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("mainPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)returnButton.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshList();
        Person person = new Person(); Company company;
        if (LoginController.getLoggedInUser().getClass() == person.getClass()){
            person = (Person)LoginController.getLoggedInUser();
            address = person.getName() + " " + person.getSurname() + "\n" +
                    person.getAddress() + "\n" +
                    person.getEmailAddress() + "\n" +
                    person.getPhoneNumber();
            addressField.setText(address);

        }
        else {
            company = (Company)LoginController.getLoggedInUser();
            address = company.getTitle() + "\n" +
                    company.getAddress() + "\n" +
                    company.getEmailAddress() + "\n" +
                    company.getPhoneNumber();
            addressField.setText(address);
        }
    }

    public void deleteBookFromOrder(ActionEvent actionEvent) {
        LoginController.getShoppingCart().removeBook(bookList.getSelectionModel().getSelectedIndex());
        refreshList();
    }

    public void refreshList () {
        bookList.getItems().clear();
        books = LoginController.getShoppingCart().getItems();
        books.forEach(book -> bookList.getItems().add(
                String.format("%-30s%-25s%s€",book.getName(),book.getAuthor(), book.getPrice())));
        priceText.setText(LoginController.getShoppingCart().calculateTotalPrice().toString()+"€");
    }
}
