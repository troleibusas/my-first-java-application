package com.example.bookshop.formControllers;

import com.example.bookshop.LoginController;
import com.example.bookshop.model.*;
import com.example.bookshop.utilities.DatabaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UserManagementController implements Initializable {
    @FXML
    public Button returnButton;
    @FXML
    public Button addUserButton;
    @FXML
    public TextField searchBar;
    @FXML
    public RadioButton isCompany;
    @FXML
    public TextField surnameField;
    @FXML
    public TextField nameField;
    @FXML
    public TextField titleField;
    @FXML
    public TextField addressField;
    @FXML
    public TextField emailField;
    @FXML
    public TextField passwordField;
    @FXML
    public TextField accCreationDate;
    @FXML
    public TextField accModifiedDate;
    @FXML
    public TextField phoneNumberField;
    @FXML
    public TextField VATField;
    @FXML
    public DatePicker birthdatePicker;
    @FXML
    public ComboBox roleBox;
    @FXML
    public ListView userList;
    @FXML
    public MenuItem deleteUser;
    @FXML
    public MenuItem addUser;
    @FXML
    public MenuItem editUser;
    @FXML
    public Button editUserButton;

    public void returnToMainScreen(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("mainPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)returnButton.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    public void addUser(boolean isEdit) throws SQLException {
        Company company = null; Person person = null;
        if (isCompany.isSelected()){
            company = new Company(phoneNumberField.getText(), emailField.getText(), VATField.getText(),
                    passwordField.getText(), (Role)roleBox.getValue(), addressField.getText(), titleField.getText());
            if (isEdit) {
                company.setDateCreated(LocalDate.parse(accCreationDate.getText()));
                company.setDateModified(LocalDate.now());
            }
            else {
                company.setDateCreated(LocalDate.now());
                company.setDateModified(LocalDate.now());
            }
            DatabaseController.addUser(company, true);
        }
        else {
            person = new Person(phoneNumberField.getText(), emailField.getText(), VATField.getText(),
                    passwordField.getText(), (Role)roleBox.getValue(), addressField.getText(), nameField.getText(),
                    surnameField.getText(), birthdatePicker.getValue());
            if (isEdit) {
                person.setDateCreated(LocalDate.parse(accCreationDate.getText()));
                person.setDateModified(LocalDate.now());
            }
            else {
                person.setDateCreated(LocalDate.now());
                person.setDateModified(LocalDate.now());
            }
            DatabaseController.addUser(person, false);
        }
        refreshScreen();
    }

    public void addUserOnClick(ActionEvent actionEvent) throws SQLException {
        addUser(false);
    }

    public void manageNameFieldsOnClick(MouseEvent actionEvent) {
        manageNameFields();
    }

    public void manageNameFields() {
        if (isCompany.isSelected()){
            titleField.setDisable(false);
            nameField.setDisable(true);
            surnameField.setDisable(true);
            birthdatePicker.setDisable(true);
        }
        else {
            titleField.setDisable(true);
            nameField.setDisable(false);
            surnameField.setDisable(false);
            birthdatePicker.setDisable(false);
        }
    }

    private void refreshScreen(){
        setDisableTextFields(true);
        addUserButton.setDisable(true);
        editUserButton.setDisable(true);
        returnButton.setDisable(false);
        clearFields();
        userList.getItems().clear();
        ArrayList<User> users = DatabaseController.getUsers("", "");
        users.forEach(user -> userList.getItems().add(user.getEmailAddress() + "\t\t" + user.getDateCreated() + "\t\t" + user.getRole()));
        roleBox.getItems().addAll(Role.values());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) { refreshScreen(); }

    public void deleteUser(ActionEvent actionEvent) {
        String selection = ((String)userList.getSelectionModel().getSelectedItem());
        String email = selection.substring(0, selection.indexOf('\t'));
        DatabaseController.deleteUser(email);
        refreshScreen();
    }

    public User returnSelectedUser(){
        Person person = new Person(); Company company = new Company();
        String selection = ((String) userList.getSelectionModel().getSelectedItem());
        String email = selection.substring(0, selection.indexOf('\t'));
        if (DatabaseController.getUsers("emailAddress", email).get(0).getClass()
                == person.getClass()){
            person = (Person)DatabaseController.getUsers("emailAddress", email).get(0);
            return person;
        }
        else{
            company = (Company) DatabaseController.getUsers("emailAddress", email).get(0);
            return company;
        }
    }

    public void showUserInfo(User user){
        clearFields();
        roleBox.getItems().setAll(Role.values());
        Person person = new Person(); Company company = new Company();
        if (user.getClass() == person.getClass()) {
            person = (Person) user;
            isCompany.setSelected(false);
            surnameField.setText(person.getSurname());
            nameField.setText(person.getName());
            addressField.setText(person.getAddress());
            emailField.setText(person.getEmailAddress());
            passwordField.setText(person.getPassword());
            accCreationDate.setText(person.getDateCreated().toString());
            accModifiedDate.setText(person.getDateModified().toString());
            phoneNumberField.setText(person.getPhoneNumber());
            VATField.setText(person.getVATCode());
            birthdatePicker.setValue(person.getBirthDate());
            roleBox.setValue(person.getRole());
        } else {
            company = (Company) user;
            isCompany.setSelected(true);
            titleField.setText(company.getTitle());
            addressField.setText(company.getAddress());
            emailField.setText(company.getEmailAddress());
            passwordField.setText(company.getPassword());
            accCreationDate.setText(company.getDateCreated().toString());
            accModifiedDate.setText(company.getDateModified().toString());
            phoneNumberField.setText(company.getPhoneNumber());
            VATField.setText(company.getVATCode());
            roleBox.setValue(company.getRole());
        }
    }

    public void showUserInfoOnClick(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            addUserButton.setDisable(true); editUserButton.setDisable(true);
            setDisableTextFields(true);
            showUserInfo(returnSelectedUser());
        }
    }

    public void clearFields () {
        nameField.clear(); surnameField.clear(); titleField.clear(); emailField.clear(); accCreationDate.clear();
        phoneNumberField.clear(); accModifiedDate.clear(); passwordField.clear(); VATField.clear(); addressField.clear();
        roleBox.getItems().clear();
        roleBox.getSelectionModel().selectFirst();
        birthdatePicker.getEditor().clear();
    }

    public void disableList(ActionEvent actionEvent) {
        userList.getSelectionModel().clearSelection();
        clearFields();
        addUserButton.setDisable(false); editUserButton.setDisable(true);
        setDisableTextFields(false);
        manageNameFields();
        roleBox.getItems().addAll(Role.values());
    }

    public void setDisableTextFields(boolean isDisabled){
        if (isDisabled){
            nameField.setDisable(true); surnameField.setDisable(true); titleField.setDisable(true); emailField.setDisable(true);
            passwordField.setDisable(true); phoneNumberField.setDisable(true); VATField.setDisable(true); addressField.setDisable(true);
            birthdatePicker.setDisable(true); roleBox.setDisable(true); isCompany.setDisable(true);
        }
        else {
            nameField.setDisable(false); surnameField.setDisable(false); titleField.setDisable(false); emailField.setDisable(false);
            passwordField.setDisable(false); phoneNumberField.setDisable(false); VATField.setDisable(false); addressField.setDisable(false);
            birthdatePicker.setDisable(false); roleBox.setDisable(false); isCompany.setDisable(false);
        }
    }

    public void editUserInfo(ActionEvent actionEvent) {
        setDisableTextFields(false);
        editUserButton.setDisable(false); returnButton.setDisable(true);
        Person person = new Person(); Company company = new Company();
        String selection = ((String)userList.getSelectionModel().getSelectedItem());
        String email = selection.substring(0, selection.indexOf('\t'));
        if (DatabaseController.getUsers("emailAddress", email).get(0).getClass()
                == person.getClass()){
            person = (Person)DatabaseController.getUsers("emailAddress", email).get(0);
            showUserInfo(person);
            DatabaseController.deleteUser(person.getEmailAddress());
        }
        else {
            company = (Company) DatabaseController.getUsers("emailAddress", email).get(0);
            showUserInfo(company);
            DatabaseController.deleteUser(company.getEmailAddress());
        }
        manageNameFields();
    }

    public void EditUser(ActionEvent actionEvent) throws SQLException {
        addUser(true);
    }
}
