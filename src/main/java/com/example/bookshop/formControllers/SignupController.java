package com.example.bookshop.formControllers;

import com.example.bookshop.LoginController;
import com.example.bookshop.model.Company;
import com.example.bookshop.model.Person;
import com.example.bookshop.model.Role;
import com.example.bookshop.utilities.DatabaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class SignupController implements Initializable {
    @FXML
    public Button returnButton;
    @FXML
    public Button saveAccButton;
    @FXML
    public CheckBox agreementCheck;
    @FXML
    public RadioButton isCompanyRadio;
    @FXML
    public TextField nameField;
    @FXML
    public TextField surnameField;
    @FXML
    public TextField emailField;
    @FXML
    public TextField addressField;
    @FXML
    public TextField titleField;
    @FXML
    public PasswordField passwordField;
    @FXML
    public TextField VATField;
    @FXML
    public DatePicker birthdatePicker;
    @FXML
    public TextField phoneNumberField;

    public void saveAndReturnToLogin(ActionEvent actionEvent) throws SQLException, IOException {
        if (agreementCheck.isSelected()) {
            if (!requiredFieldsEmpty()) {
                if ( isCompanyRadio.isSelected() ) {
                    Company company = new Company(phoneNumberField.getText(), emailField.getText(),
                            VATField.getText(), passwordField.getText(), LocalDate.now(), LocalDate.now(),
                            Role.CUSTOMER, addressField.getText(), titleField.getText());
                    DatabaseController.addUser(company, true);
                }
                else {
                    Person person = new Person(phoneNumberField.getText(), emailField.getText(),
                            VATField.getText(), passwordField.getText(), LocalDate.now(), LocalDate.now(),
                            Role.CUSTOMER, addressField.getText(), nameField.getText(), surnameField.getText(),
                            birthdatePicker.getValue());
                    DatabaseController.addUser(person, false);
                }
                returnToLoginPage();
            }
            else {
                if (isCompanyRadio.isSelected()){
                    if ( titleField.getText().isEmpty()) titleField.setStyle("-fx-background-color:#ffcccc");
                    if ( emailField.getText().isEmpty()) emailField.setStyle("-fx-background-color:#ffcccc");
                    if ( passwordField.getText().isEmpty()) passwordField.setStyle("-fx-background-color:#ffcccc");
                    if ( addressField.getText().isEmpty()) addressField.setStyle("-fx-background-color:#ffcccc");
                }
                else {
                    if ( nameField.getText().isEmpty()) nameField.setStyle("-fx-background-color:#ffcccc");
                    if ( surnameField.getText().isEmpty()) surnameField.setStyle("-fx-background-color:#ffcccc");
                    if ( emailField.getText().isEmpty()) emailField.setStyle("-fx-background-color:#ffcccc");
                    if ( passwordField.getText().isEmpty()) passwordField.setStyle("-fx-background-color:#ffcccc");
                    if ( addressField.getText().isEmpty()) addressField.setStyle("-fx-background-color:#ffcccc");
                    if ( birthdatePicker.getValue() == null) birthdatePicker.setStyle("-fx-background-color:#ffcccc");
                }
            }
        }
        else {
            agreementCheck.setTextFill(Paint.valueOf("#ff0000b7"));
        }
    }

    public void returnToLoginPage() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("loginPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)returnButton.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    public boolean requiredFieldsEmpty (){
        if (isCompanyRadio.isSelected()) {
            if (titleField.getText().isEmpty() ||
                    emailField.getText().isEmpty() ||
                    passwordField.getText().isEmpty() ||
                    addressField.getText().isEmpty()){
                return true;
            }
        }
        else {
            if ( nameField.getText().isEmpty() ||
                    surnameField.getText().isEmpty() ||
                    emailField.getText().isEmpty() ||
                    passwordField.getText().isEmpty() ||
                    addressField.getText().isEmpty() ||
                    birthdatePicker.getValue() == null){
                return true;
            }
        }
        return false;
    }

    public void manageNameFields() {
        if (isCompanyRadio.isSelected()){
            titleField.setDisable(false);
            nameField.setDisable(true);
            surnameField.setDisable(true);
            birthdatePicker.setDisable(true);
        }
        else {
            titleField.setDisable(true);
            nameField.setDisable(false);
            surnameField.setDisable(false);
            birthdatePicker.setDisable(false);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        manageNameFields();
    }
}
