package com.example.bookshop.formControllers;

import com.example.bookshop.LoginController;
import com.example.bookshop.model.Company;
import com.example.bookshop.model.Person;
import com.example.bookshop.model.User;
import com.example.bookshop.utilities.DatabaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class UserAccountController implements Initializable {
    User user = LoginController.getLoggedInUser();
    @FXML
    public Text warningText;
    @FXML
    public PasswordField password;
    @FXML
    public TextField nameField;
    @FXML
    public TextField surnameField;
    @FXML
    public TextField emailField;
    @FXML
    public TextField phonenumberField;
    @FXML
    public TextField VATField;
    @FXML
    public TextField addressField;
    @FXML
    public PasswordField newPassword;
    @FXML
    public Button saveAndReturnButton;
    @FXML
    public DatePicker datePicker;
    @FXML
    public TextField titleField;

    public void saveAndReturn(ActionEvent actionEvent) throws SQLException, IOException {
        if (user.getPassword().equals(password.getText())) {
            Person person = new Person(); Company company = new Company();
            if (user.getClass() == person.getClass()) {
                person.setName(nameField.getText());
                person.setSurname(surnameField.getText());
                person.setEmailAddress(emailField.getText());
                person.setPhoneNumber(phonenumberField.getText());
                person.setVATCode(VATField.getText());
                person.setAddress(addressField.getText());
                person.setBirthDate(datePicker.getValue());
                person.setDateCreated(user.getDateCreated());
                person.setDateModified(LocalDate.now());
                person.setRole(user.getRole());
                if (!newPassword.getText().equals("")) person.setPassword(newPassword.getText());
                else person.setPassword(password.getText());
                LoginController.setLoggedInUser(person);
                DatabaseController.deleteUser(user.getEmailAddress());
                DatabaseController.addUser(person, false);
            }
            else {
                company.setTitle(titleField.getText());
                company.setEmailAddress(emailField.getText());
                company.setPhoneNumber(phonenumberField.getText());
                company.setVATCode(VATField.getText());
                company.setAddress(addressField.getText());
                company.setDateCreated(user.getDateCreated());
                company.setDateModified(LocalDate.now());
                company.setRole(user.getRole());
                if (!newPassword.getText().equals("")) company.setPassword(newPassword.getText());
                else company.setPassword(password.getText());
                LoginController.setLoggedInUser(company);
                DatabaseController.deleteUser(user.getEmailAddress());
                DatabaseController.addUser(company, true);
            }
            FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("mainPage.fxml"));
            Parent parent = fxmlLoader.load();
            Scene scene = new Scene(parent);
            Stage stage = (Stage)saveAndReturnButton.getScene().getWindow();
            stage.setTitle("Book Store");
            stage.setScene(scene);
            stage.show();
        }
        else{
            warningText.setVisible(true);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Person person = new Person(); Company company;
        if (user.getClass() == person.getClass()) {
            titleField.setDisable(true);
            person = (Person)LoginController.getLoggedInUser();
            nameField.setText(person.getName());
            surnameField.setText(person.getSurname());
            emailField.setText(person.getEmailAddress());
            phonenumberField.setText(person.getPhoneNumber());
            VATField.setText(person.getVATCode());
            addressField.setText(person.getAddress());
            datePicker.setValue(person.getBirthDate());
        }
        else{
            nameField.setDisable(true);
            surnameField.setDisable(true);
            datePicker.setDisable(true);
            company = (Company) LoginController.getLoggedInUser();
            titleField.setText(company.getTitle());
            emailField.setText(company.getEmailAddress());
            phonenumberField.setText(company.getPhoneNumber());
            VATField.setText(company.getVATCode());
            addressField.setText(company.getAddress());
        }
    }
}
