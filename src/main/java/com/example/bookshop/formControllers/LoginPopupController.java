package com.example.bookshop.formControllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class LoginPopupController {

    @FXML
    public Button returnButton;

    public void closeWindow(ActionEvent actionEvent) {
        Stage stage = (Stage) returnButton.getScene().getWindow();
        stage.close();
    }
}
