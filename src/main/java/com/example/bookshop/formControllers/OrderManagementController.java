package com.example.bookshop.formControllers;

import com.example.bookshop.LoginController;
import com.example.bookshop.model.Order;
import com.example.bookshop.model.OrderedBook;
import com.example.bookshop.utilities.DatabaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class OrderManagementController implements Initializable {
    @FXML
    public ListView orderList;
    @FXML
    public ListView itemsList;
    @FXML
    public Button returnButton;
    @FXML
    public Text orderInfo;
    @FXML
    public MenuItem deleteOrderMenu;
    @FXML
    public MenuItem deleteItemMenu;

    public void returnToMainPage(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("mainPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)returnButton.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        orderList.getItems().addAll(DatabaseController.getOrders(""));
        orderList.getSelectionModel().selectFirst();
        showOrderItems();
    }

    public void refreshPage(){
        orderList.getItems().clear();
        itemsList.getItems().clear();
        orderList.getItems().addAll(DatabaseController.getOrders(""));
        orderList.getSelectionModel().selectFirst();
        showOrderItems();
    }

    public void showOrderItems() {
        Order selectedOrder = (Order)orderList.getSelectionModel().getSelectedItem();
        orderInfo.setText(String.valueOf(selectedOrder));
        itemsList.getItems().clear();
        itemsList.getItems().addAll(selectedOrder.getOrderedItems());
    }

    public void deleteOrder(ActionEvent actionEvent) {
        Order selectedOrder = (Order)orderList.getSelectionModel().getSelectedItem();
        DatabaseController.deleteOrder(selectedOrder.getOrderNumber());
        refreshPage();
    }

    public void deleteItem(ActionEvent actionEvent) throws SQLException {
        Order selectedOrder = (Order)orderList.getSelectionModel().getSelectedItem();
        OrderedBook selectedOrderedBook = (OrderedBook) itemsList.getSelectionModel().getSelectedItem();
        selectedOrder.getOrderedItems().remove(selectedOrderedBook);
        DatabaseController.deleteOrderedBook(selectedOrderedBook);
        selectedOrder.setTotalPrice();
        DatabaseController.deleteOrder(selectedOrder.getOrderNumber());
        DatabaseController.addOrder(selectedOrder);
        refreshPage();
    }
}
