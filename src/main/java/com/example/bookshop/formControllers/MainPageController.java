package com.example.bookshop.formControllers;

import com.example.bookshop.LoginController;
import com.example.bookshop.model.*;
import com.example.bookshop.utilities.DatabaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static java.lang.Integer.parseInt;

public class MainPageController implements Initializable {
    public int selectedComment;
    @FXML
    public TreeView bookComments;
    @FXML
    public ListView bookList;
    @FXML
    public AnchorPane bookInfo;
    @FXML
    public Button myAccButton;
    @FXML
    public Button myOrders;
    @FXML
    public TextField searchBox;
    @FXML
    public ComboBox genreFilter;
    @FXML
    public Slider priceSlider;
    @FXML
    public Button manageBooks;
    @FXML
    public Button manageUsers;
    @FXML
    public Button manageOrders;
    @FXML
    public Text nameText;
    @FXML
    public Text descriptionText;
    @FXML
    public Text priceText;
    @FXML
    public Text infoText;
    @FXML
    public Button toShoppingCartButton;
    @FXML
    public ComboBox shoppingCartList;
    @FXML
    public Button checkoutButton;
    @FXML
    public Button clearFiltersButton;
    @FXML
    public TextField replyField;
    @FXML
    public TextField commentField;
    @FXML
    public MenuItem replyToComment;
    @FXML
    public Button postComment;
    @FXML
    public MenuItem deleteComment;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshScreen();
        if (LoginController.getLoggedInUser().getRole() != Role.ADMIN){
            manageBooks.setVisible(false);
            manageUsers.setVisible(false);
            deleteComment.setVisible(false);
        }
        if (LoginController.getLoggedInUser().getRole() != Role.ADMIN && LoginController.getLoggedInUser().getRole() != Role.EMPLOYEE){
            manageOrders.setVisible(false);
        }

    }

    private void refreshScreen() {
        bookList.getItems().clear();
        ArrayList<Book> books = DatabaseController.getBooks("", "");
        books.forEach(book -> bookList.getItems().add(
                String.format("%-30s%-30s%-30s%s",book.getName(),book.getAuthor(),book.getPublisher(), book.getISBN())));
        bookList.getSelectionModel().selectFirst();
        displayBook();
        commentField.clear(); replyField.clear();
        replyField.setVisible(false);
        genreFilter.getItems().clear();
        genreFilter.getItems().addAll(Genre.values());
        List<Book> bookList = LoginController.getShoppingCart().getItems();
        for (Book book : bookList){
            shoppingCartList.getItems().add(book.getName() + "  " + book.getPrice() + "€" + "    " + book.getISBN());
        }
    }

    public void showBookManagementWindow(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("BookManagementPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)manageBooks.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    public void showUserManagementWindow(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("userManagementPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)manageBooks.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    public void showOrderManagementWindow(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("orderManagementPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)manageBooks.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    public void displayBook() {
        if (bookList.getSelectionModel().getSelectedItem() != null) {
            String selection = (String) bookList.getSelectionModel().getSelectedItem();
            String ISBN = selection.substring(selection.length() - 13);
            Book book = DatabaseController.getBooks("ISBN", ISBN).get(0);
            nameText.setText(book.getName());
            infoText.setText(String.format("%s   |   %s   |   %s   |   %s",
                    book.getAuthor(), book.getPublisher(), book.getGenre(), book.getYear()));
            descriptionText.setText(book.getDescription());
            priceText.setText(book.getPrice() + "€");
            ArrayList<Comment> comments = DatabaseController.getComments(Long.parseLong(ISBN));
            showComments(comments);
        }
    }

    public void displayBookOnClick(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            displayBook();
        }
    }

    public void showAccountManagementWindow(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("userAccountPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)manageBooks.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    public void addToShoppingCart(ActionEvent actionEvent) {
        String selection = (String)bookList.getSelectionModel().getSelectedItem();
        String ISBN = selection.substring(selection.length()-13, selection.length());
        Book tempBook = DatabaseController.getBooks("ISBN", ISBN).get(0);
        LoginController.getShoppingCart().addBook(tempBook);

        shoppingCartList.getItems().clear();
        List<Book> bookList = LoginController.getShoppingCart().getItems();
        for (Book book : bookList){
            shoppingCartList.getItems().add(book.getName() + "  " + book.getPrice() + "€" + "    " + book.getISBN());
        }

    }

    public void goToCheckout(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("checkoutPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)manageBooks.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    public void searchBooks() {
        String search = searchBox.getText();
        String genreSearch = null;
        if (genreFilter.getSelectionModel().getSelectedItem() != null) genreSearch = genreFilter.getSelectionModel().getSelectedItem().toString();
        String priceSearch = String.valueOf(priceSlider.getValue());
        bookList.getItems().clear();

        ArrayList<Book> books;
        if (genreFilter.getSelectionModel().getSelectedItem() != null) {
            books = DatabaseController.searchBook(search, genreSearch, priceSearch);
        }
        else {
            books = DatabaseController.searchBook(search, "", priceSearch);
        }

        books.forEach(book -> bookList.getItems().add(
                String.format("%-30s%-30s%-30s%s",book.getName(),book.getAuthor(),book.getPublisher(), book.getISBN())));
        bookList.getSelectionModel().selectFirst();
        displayBook();
    }

    public void clearFilters(ActionEvent actionEvent) {
        searchBox.clear();
        genreFilter.getSelectionModel().clearSelection();
        priceSlider.setValue(0.0);
        refreshScreen();
    }

    public void enableReplyField(ActionEvent actionEvent) {
        replyField.setVisible(true);
        Comment comment = (Comment)((TreeItem)bookComments.getSelectionModel().getSelectedItem()).getValue();
        selectedComment = (comment.getCommentID());

    }

    public void postComment(ActionEvent actionEvent) throws SQLException {
        String commenterName;
        Person person = new Person();
        String selection = (String)bookList.getSelectionModel().getSelectedItem();
        String bookISBN = selection.substring(selection.length()-13, selection.length());

        if ( (LoginController.getLoggedInUser().getClass() == person.getClass()))
                commenterName = ((Person)LoginController.getLoggedInUser()).getName();
        else commenterName = ((Company)LoginController.getLoggedInUser()).getTitle();

        if (!replyField.isVisible()){
            Comment comment = new Comment(commenterName, Long.parseLong(bookISBN), commentField.getText());
            DatabaseController.addComment(comment, false);
        }
        else {
            Comment comment = new Comment(commenterName, Long.parseLong(bookISBN), selectedComment, replyField.getText());
            DatabaseController.addComment(comment, true);
        }
        commentField.clear(); replyField.clear();
        replyField.setVisible(false);
        ArrayList<Comment> comments = DatabaseController.getComments(Long.parseLong(bookISBN));
        showComments(comments);
    }

    public void showComments (ArrayList<Comment> comments) {
        TreeItem<Comment> root = new TreeItem<>();
        root.setExpanded(true);
        bookComments.setRoot(root);
        for (Comment comment : comments) {
            if (comment.getRootCommentID() == 0) {
                root.getChildren().add(
                        new TreeItem<>(comment));
                for (Comment secondaryComment : comments) {
                    if (secondaryComment.getRootCommentID() == comment.getCommentID()) {
                        root.getChildren().get(root.getChildren().size()-1).getChildren().add(
                                new TreeItem<>( secondaryComment));
                    }
                }
            }
        }
    }

    public void deleteComment(ActionEvent actionEvent) {
        Comment comment = (Comment)((TreeItem)bookComments.getSelectionModel().getSelectedItem()).getValue();
        DatabaseController.deleteComment(comment.getCommentID());
        refreshScreen();
    }

    public void showUserOrdersWindow(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("userOrdersPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)manageBooks.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }
}
