package com.example.bookshop.formControllers;

import com.example.bookshop.LoginController;
import com.example.bookshop.model.*;
import com.example.bookshop.utilities.DatabaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class BookManagementController implements Initializable {
    @FXML
    public MenuItem deleteItem;
    @FXML
    public MenuItem addItem;
    @FXML
    public TextField bookName;
    @FXML
    public TextField bookAuthor;
    @FXML
    public TextField bookDescription;
    @FXML
    public ComboBox genrePicker;
    @FXML
    public TextField bookPublisher;
    @FXML
    public TextField yearPublished;
    @FXML
    public TextField bookISBN;
    @FXML
    public TextField bookLanguage;
    @FXML
    public TextField bookPrice;
    @FXML
    public Button returnButton;
    @FXML
    public TextField searchBar;
    @FXML
    public Button addButton;
    @FXML
    public ListView bookList;
    @FXML
    public MenuItem addBook;
    @FXML
    public MenuItem deleteBook;
    @FXML
    public MenuItem editItem;
    @FXML
    public Button editButton;

    public void returnToMainScreen(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LoginController.class.getResource("mainPage.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage)returnButton.getScene().getWindow();
        stage.setTitle("Book Store");
        stage.setScene(scene);
        stage.show();
    }

    public Book newBookFromFields () {
        Book book = new Book(bookName.getText(), (Genre) genrePicker.getValue(), bookDescription.getText(),
                bookAuthor.getText(), bookPublisher.getText(), Short.parseShort(yearPublished.getText()),
                Long.parseLong(bookISBN.getText()), bookLanguage.getText(), bookPrice.getText());
        return book;
    }

    public void addBook(ActionEvent actionEvent) throws SQLException {
        DatabaseController.addBook(newBookFromFields());
        refreshScreen();
    }

    private void refreshScreen(){
        setDisableTextFields(true);
        addButton.setDisable(true);
        editButton.setDisable(true);
        returnButton.setDisable(false);
        bookList.getItems().clear();
        ArrayList<Book> books = DatabaseController.getBooks("", "");
        books.forEach(book -> bookList.getItems().add(
                String.format("%-30s%-30s%-30s%s",book.getName(),book.getAuthor(),book.getPublisher(), book.getISBN())));
        clearFields();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshScreen();
    }

    public void showBookInfo(Book book) {
            clearFields();
            bookName.setText(book.getName());
            bookAuthor.setText(book.getAuthor());
            bookPublisher.setText(book.getPublisher());
            yearPublished.setText(String.valueOf(book.getYear()));
            bookISBN.setText(String.valueOf(book.getISBN()));
            bookLanguage.setText(book.getLanguage());
            bookPrice.setText(String.valueOf(book.getPrice()));
            bookDescription.setText(book.getDescription());
            genrePicker.getItems().addAll(Genre.values());
            genrePicker.setValue(book.getGenre());
    }

    public void showBookInfoOnClick(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            addButton.setDisable(true); editButton.setDisable(true);
            String selection = ((String)bookList.getSelectionModel().getSelectedItem());
            String ISBN = selection.substring(selection.length()-13, selection.length());
            Book book = DatabaseController.getBooks("ISBN", ISBN).get(0);
            showBookInfo(book);
        }
    }

    private void clearFields() {
        bookName.clear(); bookAuthor.clear(); bookPublisher.clear(); yearPublished.clear();
        bookISBN.clear(); bookLanguage.clear(); bookPrice.clear(); bookDescription.clear();
        genrePicker.getItems().clear(); genrePicker.getItems().addAll(Genre.values());
    }

    public void disableList(ActionEvent actionEvent) {
        bookList.getSelectionModel().clearSelection();
        clearFields();
        addButton.setDisable(false);
        setDisableTextFields(false);
    }

    public void deleteBook(ActionEvent actionEvent) {
        String selection = ((String)bookList.getSelectionModel().getSelectedItem());
        String ISBN = selection.substring(selection.length()-13, selection.length());
        DatabaseController.deleteBook(ISBN);
        refreshScreen();
    }

    public void editItemInfo(ActionEvent actionEvent) {
        editButton.setDisable(false); returnButton.setDisable(true);
        setDisableTextFields(false);
        String selection = ((String)bookList.getSelectionModel().getSelectedItem());
        String ISBN = selection.substring(selection.length()-13, selection.length());
        Book originalBook = DatabaseController.getBooks("ISBN", ISBN).get(0);
        showBookInfo(originalBook);
        DatabaseController.deleteBook(String.valueOf(originalBook.getISBN()));
    }

    public void setDisableTextFields(boolean isDisabled){
        if (isDisabled){
            bookName.setDisable(true); bookAuthor.setDisable(true); bookPublisher.setDisable(true); yearPublished.setDisable(true);
            bookISBN.setDisable(true); bookLanguage.setDisable(true);bookPrice.setDisable(true); bookDescription.setDisable(true);
            genrePicker.setDisable(true);
        }
        else {
            bookName.setDisable(false); bookAuthor.setDisable(false); bookPublisher.setDisable(false); yearPublished.setDisable(false);
            bookISBN.setDisable(false); bookLanguage.setDisable(false);bookPrice.setDisable(false); bookDescription.setDisable(false);
            genrePicker.setDisable(false);
        }
    }
}
