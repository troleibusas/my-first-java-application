package com.example.bookshop.model;

import com.example.bookshop.utilities.DatabaseController;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Order {
    private String customerID;
    private String address;
    private PaymentMethod paymentMethod;
    private LocalDate dateCreated;
    private BigDecimal totalPrice;
    private ArrayList<OrderedBook> orderedItems;
    private int orderNumber;

    public Order() {
    }

    public String getCustomerID() {
        return customerID;
    }

    public String getAddress() {
        return address;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public ArrayList<OrderedBook> getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(ArrayList<OrderedBook> orderedItems) {
        this.orderedItems = orderedItems;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public Order(ArrayList<Book> items, String customerID, String address, String totalPrice, PaymentMethod paymentMethod) {
        this.customerID = customerID;
        this.address = address;
        this.totalPrice = new BigDecimal(totalPrice);
        this.paymentMethod = paymentMethod;
        orderNumber = ThreadLocalRandom.current().nextInt(100000, Integer.MAX_VALUE);
        orderedItems = new ArrayList<>();
        items.forEach(book -> orderedItems.add(new OrderedBook(orderNumber, book.getISBN())));
        dateCreated = LocalDate.now();
    }

    public Order(String customerID, ArrayList<OrderedBook> orderedBooks, String address, PaymentMethod paymentMethod) {
        this.customerID = customerID;
        this.address = address;
        this.paymentMethod = paymentMethod;
        dateCreated = LocalDate.now();
        orderNumber = ThreadLocalRandom.current().nextInt(100000, Integer.MAX_VALUE);
    }

    public Order(String customerID, int orderNumber, LocalDate dateCreated, String address, String totalPrice, PaymentMethod paymentMethod) {
        this.customerID = customerID;
        this.orderNumber = orderNumber;
        this.address = address;
        this.totalPrice = new BigDecimal(totalPrice);
        this.paymentMethod = paymentMethod;
        this.dateCreated = dateCreated;
    }

    public void setTotalPrice() {
        BigDecimal temp = BigDecimal.ZERO;
        for (OrderedBook book : orderedItems){
            Book bookInfo = DatabaseController.getBooks("ISBN", String.valueOf(book.getISBN())).get(0);
            temp = temp.add(bookInfo.getPrice());
        }
        totalPrice = temp;
    }

    @Override
    public String toString() {
        return orderNumber + "\t" + customerID + "\t" + dateCreated + "\t" + totalPrice + "€";
    }
}
