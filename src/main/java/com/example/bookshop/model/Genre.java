package com.example.bookshop.model;

public enum Genre {
    DRAMA,
    CLASSIC,
    ADVENTURE,
    DETECTIVE,
    FANTASY,
    FICTION,
    HORROR,
    ROMANCE,
    SCI_FI,
    BIOGRAPHY,
    EDUCATIONAL,
    LANGUAGES
}
