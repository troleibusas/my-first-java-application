package com.example.bookshop.model;

public enum PaymentMethod {
    CASH,
    CARD,
    BANK,
    PAYPAL
}
