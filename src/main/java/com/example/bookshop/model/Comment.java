package com.example.bookshop.model;

public class Comment {
    private String commenterName;
    private long bookISBN;
    private int commentID;
    private int rootCommentID;
    private String commentContent;

    public Comment(String commenterName, long bookISBN, int commentID, int rootCommentID, String commentContent) {
        this.commenterName = commenterName;
        this.bookISBN = bookISBN;
        this.commentID = commentID;
        this.rootCommentID = rootCommentID;
        this.commentContent = commentContent;
    }

    public Comment(String commenterName, long bookISBN, int rootCommentID, String commentContent) {
        this.commenterName = commenterName;
        this.bookISBN = bookISBN;
        this.rootCommentID = rootCommentID;
        this.commentContent = commentContent;
    }

    public Comment(String commenterName, long bookISBN, String commentContent) {
        this.commenterName = commenterName;
        this.bookISBN = bookISBN;
        this.commentContent = commentContent;
    }

    public Comment() {
    }

    public long getBookISBN() {
        return bookISBN;
    }

    public void setBookISBN(long bookISBN) {
        this.bookISBN = bookISBN;
    }

    public int getRootCommentID() {
        return rootCommentID;
    }

    public void setRootCommentID(int rootCommentID) {
        this.rootCommentID = rootCommentID;
    }

    public String getCommenterName() {
        return commenterName;
    }

    public void setCommenterName(String commenterName) {
        this.commenterName = commenterName;
    }

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    @Override
    public String toString() {
        return commentID + "\t\t" + commenterName + "\t" + commentContent;
    }
}
