package com.example.bookshop.model;

public enum Role {
    EMPLOYEE,
    CUSTOMER,
    ADMIN
}
