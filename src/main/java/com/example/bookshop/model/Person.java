package com.example.bookshop.model;

import java.time.LocalDate;

public class Person extends User {

    private String name;
    private String surname;
    private LocalDate birthDate;

    public Person(String phoneNumber, String emailAddress, String VATCode, String password, Role role,
                  String address, String name, String surname, LocalDate birthDate) {
        super(phoneNumber, emailAddress, VATCode, password, role, address);
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Person(String phoneNumber, String emailAddress, String VATCode, String password, LocalDate dateCreated, LocalDate dateModified, Role role, String address, String name, String surname, LocalDate birthDate) {
        super(phoneNumber, emailAddress, VATCode, password, dateCreated, dateModified, role, address);
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Person() {
        super();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }



    @Override
    public String toString() {
        return "Person{" +
                "name = '" + name + '\'' +
                ", surname = '" + surname + '\'' +
                ", birthDate = " + birthDate +
                ", role = " + role +
                ", email =" + getEmailAddress() +
                ", phoneNumber = " + getPhoneNumber() +
                '}';
    }
}
