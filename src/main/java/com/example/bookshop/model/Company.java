package com.example.bookshop.model;

import java.time.LocalDate;

public class Company extends User {

    private String title;

    public Company(String phoneNumber, String emailAddress, String VATCode, String password,
                   Role role, String address, String title) {
        super(phoneNumber, emailAddress, VATCode, password, role, address);
        this.title = title;
    }

    public Company(String phoneNumber, String emailAddress, String VATCode, String password, LocalDate dateCreated, LocalDate dateModified, Role role, String address, String title) {
        super(phoneNumber, emailAddress, VATCode, password, dateCreated, dateModified, role, address);
        this.title = title;
    }

    public Company() {
        super();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Company{" +
                "title = '" + title + '\'' +
                ", email = " + getEmailAddress() +
                '}';
    }
}
