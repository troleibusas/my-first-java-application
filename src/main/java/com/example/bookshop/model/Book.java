package com.example.bookshop.model;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;

public class Book {
    private String name;
    private String description;
    private String author;
    private String publisher;
    private short year;
    private long ISBN;
    private String language;
    private BigDecimal price;
    private Genre genre;

    public Book(String name, Genre genre, String description, String author,
                String publisher, short year, long ISBN,
                String language, String price) {
        this.name = name;
        this.genre = genre;
        this.description = description;
        this.author = author;
        this.publisher = publisher;
        this.year = year;
        this.ISBN = ISBN;
        this.language = language;
        this.price = new BigDecimal(price);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public short getYear() {
        return year;
    }

    public void setYear(short year) {
        this.year = year;
    }

    public long getISBN() {
        return ISBN;
    }

    public void setISBN(long ISBN) {
        this.ISBN = ISBN;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' + '\n' +
                ", description='" + description + '\'' + '\n' +
                ", author='" + author + '\'' + '\n' +
                ", publisher='" + publisher + '\'' + '\n' +
                ", year=" + year + '\n' +
                ", ISBN=" + ISBN + '\n' +
                ", language='" + language + '\'' + '\n' +
                ", price=" + price + '\n' +
                '}';
    }
}
