package com.example.bookshop.model;


import java.math.BigDecimal;
import java.util.ArrayList;

public class ShoppingCart {
    private ArrayList<Book> items;

    public ShoppingCart() {
        items = new ArrayList<Book>();
    }

    public ArrayList<Book> getItems() {
        return items;
    }

    public BigDecimal calculateTotalPrice() {
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Book book : items) {
            totalPrice = totalPrice.add(book.getPrice());
        }
        return totalPrice;
    }

    public void addBook(Book book){
        items.add(book);
    }

    public void removeBook(int index){
        items.remove(index);
    }

    public void clear(){
        items.clear();
    }
    @Override
    public String toString() {
        return "ShoppingCart{" +
                "totalPrice = " + calculateTotalPrice() +
                '}';
    }
}
