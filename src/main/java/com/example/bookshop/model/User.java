package com.example.bookshop.model;

import java.time.LocalDate;

public class User {
    private String phoneNumber;
    private String emailAddress;
    private String VATCode;
    private String password;
    private LocalDate dateCreated;
    private LocalDate dateModified;
    Role role;
    private String address;


    public User(String phoneNumber, String emailAddress, String VATCode, String password, Role role, String address) {
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.VATCode = VATCode;
        this.password = password;
        this.role = role;
        this.address = address;
        this.dateModified = LocalDate.now();
        this.dateCreated = LocalDate.now();
    }

    public User(String phoneNumber, String emailAddress, String VATCode, String password, LocalDate dateCreated, LocalDate dateModified, Role role, String address) {
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.VATCode = VATCode;
        this.password = password;
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
        this.role = role;
        this.address = address;
    }

    public User() {

    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getVATCode() {
        return VATCode;
    }

    public void setVATCode(String VATCode) {
        this.VATCode = VATCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDate getDateModified() {
        return dateModified;
    }

    public void setDateModified(LocalDate dateModified) {
        this.dateModified = dateModified;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", VATCode='" + VATCode + '\'' +
                ", password='" + password + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateModified=" + dateModified +
                ", role=" + role +
                ", address='" + address + '\'' +
                '}';
    }
}
