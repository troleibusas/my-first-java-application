package com.example.bookshop.model;

import com.example.bookshop.utilities.DatabaseController;

public class OrderedBook {

    private int orderNumber;
    private long ISBN;
    private int bookID;

    public OrderedBook(int orderNumber, long ISBN, int bookID) {
        this.orderNumber = orderNumber;
        this.ISBN = ISBN;
        this.bookID = bookID;
    }
    public OrderedBook(int orderNumber, long ISBN) {
        this.orderNumber = orderNumber;
        this.ISBN = ISBN;
    }

    public OrderedBook() {

    }

    public long getISBN() {
        return ISBN;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public int getBookID() {
        return bookID;
    }

    @Override
    public String toString() {
        Book book = DatabaseController.getBooks("ISBN", String.valueOf(ISBN)).get(0);
        return book.getISBN() + "\t" + book.getName() + "\t" + book.getAuthor() + "\t" + book.getPrice();
    }
}
